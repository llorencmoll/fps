﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParasiteGenerator : MonoBehaviour {

	public GameObject Parasite;
	public Transform[] points;
	public float timeToSpawn;

	IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);

			GameObject iParasite = Instantiate (Parasite);
			ParasiteBehaviour Pb = iParasite.GetComponent<ParasiteBehaviour> ();
			Pb.pathNodes = points;
		}
	}
}
