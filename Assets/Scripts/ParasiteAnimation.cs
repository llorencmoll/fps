﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParasiteAnimation : MonoBehaviour {

	ParasiteBehaviour Parasite;
	private void Start()
	{
		Parasite = GetComponentInParent<ParasiteBehaviour>();
	}
	public void EndExplosion()
	{
		Debug.Log("Explosion!!!");
		Parasite.Explosion();
	}
}
